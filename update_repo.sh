#!/bin/bash
./create_repository.py \
        https://gitlab.com/Phoenix-addons/addons-repo.git#master:repository.mpdb \
	https://gitlab.com/Phoenix-addons/addons-repo.git#master:docker.mpdb.mpdmanager \
	https://github.com/iconmix/skins-addons.git#master:skin.iconmix-krypton \
        https://github.com/iconmix/skins-addons.git#master:skin.iconmix-jarvis \
        https://github.com/iconmix/skins-addons.git#master:script.iconmixtools

MESS=$(git add -nvA | grep zip)
git add -A
git commit -a -m "$MESS"
git push
